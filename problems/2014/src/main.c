#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void theReturnOfKing(char *grades) {
  double sum = 0;
  int students = 0;
  int i;
  for (i = 0; i < strlen(grades); i++) {
    if (grades[i] == '1' && i < strlen(grades) && grades[i + 1] == '0') {
      sum += 10;
      i += 1;
    } else if (grades[i] == '1' && i < strlen(grades)) {
      sum += 1;
    } else {
      char a[2];
      a[0] = grades[i];
      a[1] = '\0';
      sum += atoi(a);
    }
    students += 1;
  }
  printf("AVG: %f\n", sum / students);
}

void blackAndWhiteStones(char *stones, int a, int b) {
  int coins = 0, n = 0;
  char *s = stones;
  void switchStones(char *stones) {
    char c = stones[0];
    stones[0] = stones[1];
    stones[1] = c;
    coins += (a - b);
  }
  void orderStones(char *stones) {
    printf("%d: %s\n", n++, stones);
    if (stones[0] == 'w') {
      if (stones[1] == 'w') {
        orderStones(&stones[1]);
      }
      if (stones[1] == 'b') {
        switchStones(stones);
        orderStones(&stones[1]);
      }
    } else if (stones[0] == 'b') {
      orderStones(&stones[1]);
    }
  }
  orderStones(stones);
  printf("%s\n", stones);
  printf("Coins lost: %d\n", coins);
}

int main(int argc, char **argv) {
  if (argc == 2) {
    char *grades = argv[1];
    theReturnOfKing(grades);
  } else {
    printf("No grades found!\n");
  }
  char a[] = "wbwwbwbwbwbbbwwbbb";
  blackAndWhiteStones(a, 5, 3);
  return 0;
}
